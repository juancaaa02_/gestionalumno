package gestionAlumnos.Controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import gestionAlumnos.Consultas.Consultas;
import jakarta.annotation.Resource;

/**
 * Servlet implementation class Controlador
 */
public class ControladorMatricula extends HttpServlet   {
	private static final long serialVersionUID = 1L;
       
	@Autowired
	private Consultas consulta;

	
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//Leer el parametro que le llega del formulario.
		
		String elComando=request.getParameter("instruccion");
		
		
		//Si no se envia el parametro, listar los productos
		
		if(elComando==null) 
			elComando="matricular_alumno";
		
		
		
		//Si ha resivido el parametro o no, redirigir el flujo al metodo adecuado 
		
		switch (elComando) {
		
		case "matricular_alumno":
			
			try {
				consulta.matricularAlumno(null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		
	}
	}
}
