package gestionAlumno.DAL;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 * @author juanca
 * AlumnoDAL: Entidad que refiera a la tabla de base de datos matricula_alumno.
 * Registros de la recepción de la matricula del alumno
 */

/*
 * IMPORTANTE: La base de datos y el esquema si hay que crearlo. La tabla se generará sola.
 */
@Entity
@Table(name="matricula_alumno",schema = "gestion_portatil_alumno")
public class AlumnoDAL {

	
		//Atributos 
	
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name="id_alumno")
		private int id_alumno;
		@Column(name="nombre_alumno")
		private String nombre_alumno;
		@Column(name="telefono_alumno")
		private String telefono_alumno;
		@OneToOne
		PortatilDAL portatil;
		
		
		//Metodos Getters and Setters 
		
		public int getId_alumno() {
			return id_alumno;
		}
		public void setId_alumno(int id_alumno) {
			this.id_alumno = id_alumno;
		}
		public String getNombre_alumno() {
			return nombre_alumno;
		}
		public void setNombre_alumno(String nombre_alumno) {
			this.nombre_alumno = nombre_alumno;
		}
		public String getTelefono_alumno() {
			return telefono_alumno;
		}
		public void setTelefono_alumno(String telefono_alumno) {
			this.telefono_alumno = telefono_alumno;
		}
		
		
		//Constructores
		

		public AlumnoDAL(int id_alumno, String nombre_alumno, String telefono_alumno) {
			super();
			this.id_alumno = id_alumno;
			this.nombre_alumno = nombre_alumno;
			this.telefono_alumno = telefono_alumno;
			
		}
		
		public AlumnoDAL() {
			
		}
}
