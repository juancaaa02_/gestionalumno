package gestionAlumno.DAL;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 * @author juanca
 * PortatilDAL: Entidad que refiera a la tabla de base de datos portatil_alumno.
 * Registros de la recepción de la matricula del alumno
 */

/*
 * IMPORTANTE: La base de datos y el esquema si hay que crearlo. La tabla se generará sola.
 */
@Entity
@Table(name="portatil_alumno",schema = "gestion_portatil_alumno")
public class PortatilDAL {

	
	//Entidades
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_portatil")
	private int id_portatil;
	@Column(name="marca_portatil")
	private String marca_portatil;
	@Column(name="modelo_portatil")
	private String modelo_portatil;
	@OneToOne
	AlumnoDAL dueño_portatil;
	
	//Getters and Setters
	
	public int getId_portatil() {
		return id_portatil;
	}
	public void setId_portatil(int id_portatil) {
		this.id_portatil = id_portatil;
	}
	public String getMarca_portatil() {
		return marca_portatil;
	}
	public void setMarca_portatil(String marca_portatil) {
		this.marca_portatil = marca_portatil;
	}
	public String getModelo_portatil() {
		return modelo_portatil;
	}
	public void setModelo_portatil(String modelo_portatil) {
		this.modelo_portatil = modelo_portatil;
	}
	public AlumnoDAL getDueño_portatil() {
		return dueño_portatil;
	}
	public void setDueño_portatil(AlumnoDAL dueño_portatil) {
		this.dueño_portatil = dueño_portatil;
	}
	
	//Constructores
	
	public PortatilDAL(int id_portatil, String marca_portatil, String modelo_portatil, AlumnoDAL dueño_portatil) {
		super();
		this.id_portatil = id_portatil;
		this.marca_portatil = marca_portatil;
		this.modelo_portatil = modelo_portatil;
		this.dueño_portatil = dueño_portatil;
	}
	public PortatilDAL() {
		super();
	}
	
	
	
}
