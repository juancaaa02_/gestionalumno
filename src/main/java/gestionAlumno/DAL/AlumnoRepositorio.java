package gestionAlumno.DAL;

/**
 * @author juanca
 * AlumnoRepositorio: Interfaz con las operaciones básicas contra base de datos para la tabla matricula_alumno
 */

public interface AlumnoRepositorio {

	public void altaAlumno (AlumnoDAL alumno) throws Exception;
	
	public void bajaAlumno(int id_alumno) throws Exception;
	
}
