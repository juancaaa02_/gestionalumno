package gestionAlumno.DAL;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

/**
 * @author juanca
 * Implementación de la interfaz AlumnoRepositorio
 */

@Repository
public class AlumnoRepositorioImpl implements AlumnoRepositorio {

	
	@PersistenceContext
	private EntityManager em;
	
	public void altaAlumno(AlumnoDAL alumno) throws Exception {
		
		em.persist(alumno);
		em.clear();
		em.close();
		
	}

	public void bajaAlumno(int id_alumno) throws Exception {
		
		String jpql = "SELECT matricula_alumno ma DELETE AlumnoDAL ma WHERE ma.id_alumno >?id_alumno";
		Query query =em.createQuery(jpql);
		query.setParameter("id_alumno",id_alumno);
	    query.executeUpdate();
		
	}
}
