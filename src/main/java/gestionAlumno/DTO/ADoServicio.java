package gestionAlumno.DTO;

import gestionAlumno.DAL.AlumnoDAL;
import gestionAlumno.DAL.PortatilDAL;

/**
 * @author Juanca
 * ADaoServicio: Interfaz con los métoso que pasan de DTO a DAO.
 */

public interface ADoServicio {

	public AlumnoDAL AlumnoNormalDTOaDAO(AlumnoDTO alumnoDTO);
	
	public PortatilDAL PortatilNormalDTOaDAO(PortatilDTO portatilDTO);
}
