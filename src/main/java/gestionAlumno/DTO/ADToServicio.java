package gestionAlumno.DTO;
/**
 * @author Juanca
 * ADtoServicio: Interfaz con los métoso que pasan de DAO a DTO.
 */
public interface ADToServicio {

	
	public  AlumnoDTO AalumnoDTONormal(int id_alumno,String nombre_alumno,String numero_alumno,int id_portatil);
	public PortatilDTO APortatilDTONormal(int id_portatil,String marca_portatil,String modelo_portatil);
}
