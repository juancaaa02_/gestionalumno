package gestionAlumno.DTO;


import org.springframework.stereotype.Service;

/**
 * @author Juanca
 * Interfaz que implementa la case ADtoServicio
 */

@Service
public class ADToServicioImpl implements ADToServicio {

	public AlumnoDTO AalumnoDTONormal(int id_alumno, String nombre_alumno, String numero_alumno,int id_portatil) {
		
	
		AlumnoDTO alumDTONormal = new AlumnoDTO(id_alumno, nombre_alumno, numero_alumno,id_portatil);
		
		return alumDTONormal;
	}

	public PortatilDTO APortatilDTONormal(int id_portatil, String marca_portatil, String modelo_portatil) {
		
		PortatilDTO portatilDTONormal = new PortatilDTO(id_portatil, marca_portatil, modelo_portatil);
		
		return portatilDTONormal;
	}
	
	

}
