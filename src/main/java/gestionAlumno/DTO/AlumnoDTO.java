package gestionAlumno.DTO;

import org.springframework.stereotype.Component;

@Component
public class AlumnoDTO {

	
	//Atributos 
	
	private int id_alumno;
	private String nombre_alumno;
	private String telefono_alumno;
	private int id_portatil;
	
	//Metodo Getters and Setters 
	
	public int getId_alumno() {
		return id_alumno;
	}
	public void setId_alumno(int id_alumno) {
		this.id_alumno = id_alumno;
	}
	public String getNombre_alumno() {
		return nombre_alumno;
	}
	public void setNombre_alumno(String nombre_alumno) {
		this.nombre_alumno = nombre_alumno;
	}
	public String getTelefono_alumno() {
		return telefono_alumno;
	}
	public void setTelefono_alumno(String telefono_alumno) {
		this.telefono_alumno = telefono_alumno;
	}
	public int getId_portatil() {
		return id_portatil;
	}
	public void setId_portatil(int id_portatil) {
		this.id_portatil = id_portatil;
	}
	
	//Constructores 
	
	public AlumnoDTO(int id_alumno, String nombre_alumno, String telefono_alumno, int id_portatil) {
		super();
		this.id_alumno = id_alumno;
		this.nombre_alumno = nombre_alumno;
		this.telefono_alumno = telefono_alumno;
		this.id_portatil = id_portatil;
	}
	public AlumnoDTO() {
		super();
	}
	

	
	
}
