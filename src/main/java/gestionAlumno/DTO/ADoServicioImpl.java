package gestionAlumno.DTO;

import org.springframework.stereotype.Service;

import gestionAlumno.DAL.AlumnoDAL;
import gestionAlumno.DAL.PortatilDAL;

/**
 * @author Juanca
 * Implementación de la interfaz ADaoServicio
 */

@Service
public class ADoServicioImpl implements ADoServicio {

	public AlumnoDAL AlumnoNormalDTOaDAO(AlumnoDTO alumnoDTO) {
		
		AlumnoDAL alumnoDAL = new AlumnoDAL();
		
		if(alumnoDTO !=null) {
			
			alumnoDAL.setId_alumno(alumnoDTO.getId_alumno());
			alumnoDAL.setNombre_alumno(alumnoDTO.getNombre_alumno());
			alumnoDAL.setTelefono_alumno(alumnoDTO.getTelefono_alumno());
		}
		
			return alumnoDAL;
	}

	public PortatilDAL PortatilNormalDTOaDAO(PortatilDTO portatilDTO) {
	
		PortatilDAL portatilDAL = new PortatilDAL();
		
		if(portatilDTO != null) {
			
			portatilDAL.setId_portatil(portatilDTO.getId_portatil());
			portatilDAL.setMarca_portatil(portatilDTO.getMarca_portatil());
			portatilDAL.setModelo_portatil(portatilDTO.getModelo_portatil());
		}
		
		return portatilDAL;
	}

	

}
