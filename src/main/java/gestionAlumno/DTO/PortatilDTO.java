package gestionAlumno.DTO;

public class PortatilDTO {

	//Atributos
	
	private int id_portatil;
	private String marca_portatil;
	private String modelo_portatil;
	
	//Getters and Setters 
	
	public int getId_portatil() {
		return id_portatil;
	}
	public void setId_portatil(int id_portatil) {
		this.id_portatil = id_portatil;
	}
	public String getMarca_portatil() {
		return marca_portatil;
	}
	public void setMarca_portatil(String marca_portatil) {
		this.marca_portatil = marca_portatil;
	}
	public String getModelo_portatil() {
		return modelo_portatil;
	}
	public void setModelo_portatil(String modelo_portatil) {
		this.modelo_portatil = modelo_portatil;
	}
	
	//Contructores
	
	public PortatilDTO(int id_portatil, String marca_portatil, String modelo_portatil) {
		super();
		this.id_portatil = id_portatil;
		this.marca_portatil = marca_portatil;
		this.modelo_portatil = modelo_portatil;
	}
	
	public PortatilDTO() {
		
	}
	
	
}
